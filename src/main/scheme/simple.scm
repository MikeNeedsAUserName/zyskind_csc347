; INSTRUCTIONS
; 
; Type in all of the expressions (a)-(o) from Exercise 2.2.3 of 
; http://www.scheme.com/tspl4/start.html   
; For each expression, type in its value beneath the expression (as a comment).  
; Use https://repl.it/languages/Scheme/ or SISC Scheme to verify your answers (see worksheet 1).
; (a) has been completed for you.
; 
; SUBMISSION
;
; 1. Push your local repository to the repository created for you on Bitbucket before the deadline.
;
; 2. Late submissions will not be permitted because solutions will be discussed in class.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Exercise 2.2.3(a)

(cons 'car 'cdr)
; evaluates to: (car . cdr)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Exercise 2.2.3(b)

(list 'this '(is silly))   ;'(is silly) evaluates to (is silly)
; evaluates to: (this (is silly))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Exercise 2.2.3(c)

(cons 'is '(this silly?))
; evaluates to: (is this silly?)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Exercise 2.2.3(d)

(quote (+ 2 3))
; evaluates to: (+ 2 3)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Exercise 2.2.3(e)

(cons '+ '(2 3))
; evaluates to: (+ 2 3)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Exercise 2.2.3(f)

(car '(+ 2 3))
; evaluates to: +

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Exercise 2.2.3(g)

(cdr '(+ 2 3))
; evaluates to: (2 3)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Exercise 2.2.3(h)

cons
; evaluates to: #<Function cons>

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Exercise 2.2.3(i)

(quote cons)
; evaluates to: cons

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Exercise 2.2.3(j)

(quote (quote cons))
; evaluates to: (quote cons)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Exercise 2.2.3(k)

(car (quote (quote cons))) ;(quote (quote cons)) evaluates to (quote cons) and car will return the left item
; evaluates to: quote

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Exercise 2.2.3(l)

(+ 2 3)
; evaluates to: 5

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Exercise 2.2.3(m)

(+ '2 '3)
; evaluates to: 5

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Exercise 2.2.3(n)

(+ (car '(2 3)) (car (cdr '(2 3)))) ;add 2 because of the car expression on the pair (2 3) then (cdr (2 3)) returns 3
; evaluates to: 5

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Exercise 2.2.3(o)

((car (list + - * /)) 2 3) ;create list of arithmetic operators and take the first one
; evaluates to: 5

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

